FROM benmarcll/frontend:latest as frontend

FROM maven:3.8.2-openjdk-11-slim as build
COPY src /home/app/src
COPY pom.xml /home/app
COPY --from=frontend /usr/local/app/dist/bene-marcell-emszhw-szakdolgozat-frontend/ /home/app/src/main/resources/static
RUN mvn -f /home/app/pom.xml clean package -P vps-prod

FROM openjdk:11-jdk-slim
COPY --from=build /home/app/target/szakdolgozat-0.0.1-SNAPSHOT.jar /usr/local/lib/app.jar
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=vps-prod","/usr/local/lib/app.jar"]
