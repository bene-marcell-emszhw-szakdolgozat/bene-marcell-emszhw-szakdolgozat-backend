package pte.mik.szakdolgozat.entity;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Document
@NoArgsConstructor
@AllArgsConstructor
public class Email {

    @Id
    private String id;
    private LocalDateTime sentDate;
    private List<String> inReplyTo;
    private List<String> replyReferences;
    private String subject;
    private String from;
    private List<String> to;
    private List<String> cc;
    private List<String> bcc;
    private String plainContent;
    private String htmlContent;
    private List<Attachment> attachments;
    private String conversationId;
}
