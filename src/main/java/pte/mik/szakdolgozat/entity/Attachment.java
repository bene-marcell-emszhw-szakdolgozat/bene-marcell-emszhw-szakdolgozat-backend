package pte.mik.szakdolgozat.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Attachment implements Serializable {

    private static final long serialVersionUID = 6736901379148502166L;
    private byte[] data;
    private String contentType;
    private String name;

}
