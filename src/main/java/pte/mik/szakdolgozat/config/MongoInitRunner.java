package pte.mik.szakdolgozat.config;

import java.text.MessageFormat;

import org.springframework.boot.CommandLineRunner;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequiredArgsConstructor
public class MongoInitRunner implements CommandLineRunner {

    private static final String MONGODB_EMAIL_COLLECTION_NAME = "email";
    private static final Long MONGODB_EMAIL_COLLECTION_SIZE = 16777216L;
    private static final Long MONGODB_EMAIL_COLLECTION_MAX_DOCUMENTS = 2147483647L;

    private final ReactiveMongoTemplate reactiveMongoTemplate;

    @Override
    public void run(String... args) {
        this.reactiveMongoTemplate.collectionExists(MONGODB_EMAIL_COLLECTION_NAME).map(isExists -> {
            if (!isExists) {
                this.reactiveMongoTemplate.createCollection(MONGODB_EMAIL_COLLECTION_NAME,
                        CollectionOptions.empty()
                                         .capped()
                                         .size(MONGODB_EMAIL_COLLECTION_SIZE)
                                         .maxDocuments(MONGODB_EMAIL_COLLECTION_MAX_DOCUMENTS)).subscribe();
                return MessageFormat.format(
                        "A new collection has been created with the name of \"{0}\", with the size of {1}. Max number of documents: {2}",
                        MONGODB_EMAIL_COLLECTION_NAME, MONGODB_EMAIL_COLLECTION_SIZE,
                        MONGODB_EMAIL_COLLECTION_MAX_DOCUMENTS);
            } else {
                return MessageFormat.format("A collection with the name of \"{0}\" has already existed.", MONGODB_EMAIL_COLLECTION_NAME);
            }

        }).subscribe(log::info);
    }
}
