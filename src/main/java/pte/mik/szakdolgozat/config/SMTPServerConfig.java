package pte.mik.szakdolgozat.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.subethamail.smtp.helper.SimpleMessageListener;
import org.subethamail.smtp.server.SMTPServer;

import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Setter
@Configuration
@RequiredArgsConstructor
@ConfigurationPropertiesScan
@ConfigurationProperties(prefix = "smtp")
public class SMTPServerConfig {

    private int port;
    private final SimpleMessageListener simpleMessageListener;

    @Bean("smtpServer")
    public SMTPServer smtpServer() {
        return SMTPServer
                .port(this.port)
                .simpleMessageListener(this.simpleMessageListener)
                .build();
    }


}
