package pte.mik.szakdolgozat.config;

import java.io.InputStream;

import org.springframework.context.annotation.Configuration;
import org.subethamail.smtp.helper.SimpleMessageListener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pte.mik.szakdolgozat.entity.Email;
import pte.mik.szakdolgozat.service.EmailService;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class SimpleMessageListenerImpl implements SimpleMessageListener {

    private final EmailService emailService;

    @Override
    public boolean accept(String from, String recipient) {
        return true;
    }

    @Override
    public void deliver(String from, String recipient, InputStream data) {

        try {

            Email deliveredEmail = this.emailService.inputStreamToEmail(data);

            log.info("Delivered message..." + deliveredEmail);

            this.emailService.save(deliveredEmail).subscribe();

        } catch (Exception e) {

            log.error(e.getMessage(), e);

        }

    }

}
