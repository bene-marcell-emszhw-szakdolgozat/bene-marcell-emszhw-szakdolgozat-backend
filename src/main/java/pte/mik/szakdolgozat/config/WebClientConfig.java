package pte.mik.szakdolgozat.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.reactive.function.client.WebClientCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.*;
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.reactive.function.client.ServerOAuth2AuthorizedClientExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientConfig {

    @Bean
    public WebClient keycloakWebClient(WebClientCustomizer oauth2WebClientCustomizer,
            @Value("${keycloak.api.admin-base-url}") String baseUrlForKeycloakAdminApi) {

        WebClient.Builder webClientBuilder = WebClient.builder().baseUrl(baseUrlForKeycloakAdminApi);
        oauth2WebClientCustomizer.customize(webClientBuilder);

        return webClientBuilder.build();
    }

    @Bean
    public WebClientCustomizer oauth2WebClientCustomizer(
            ReactiveOAuth2AuthorizedClientManager reactiveOAuth2AuthorizedClientManager,
            @Value("${spring.security.oauth2.client.registration.keycloak-admin.client-id}") String keycloakAdminApiClient) {
        ServerOAuth2AuthorizedClientExchangeFilterFunction oAuth2AuthorizedClientExchangeFilterFunction = new ServerOAuth2AuthorizedClientExchangeFilterFunction(
                reactiveOAuth2AuthorizedClientManager);

        oAuth2AuthorizedClientExchangeFilterFunction.setDefaultClientRegistrationId(keycloakAdminApiClient);

        return webClientBuilder -> webClientBuilder.filter(oAuth2AuthorizedClientExchangeFilterFunction);
    }

    @Bean
    public ReactiveOAuth2AuthorizedClientManager authorizedClientManager(
            ReactiveClientRegistrationRepository clientRegistrationRepository,
            ReactiveOAuth2AuthorizedClientService authorizedClientService) {

        ReactiveOAuth2AuthorizedClientProvider authorizedClientProvider = ReactiveOAuth2AuthorizedClientProviderBuilder.builder()
                                                                                                                       .clientCredentials()
                                                                                                                       .build();

        AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager authorizedClientManager = new AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager(
                clientRegistrationRepository, authorizedClientService);
        authorizedClientManager.setAuthorizedClientProvider(authorizedClientProvider);

        return authorizedClientManager;
    }
}
