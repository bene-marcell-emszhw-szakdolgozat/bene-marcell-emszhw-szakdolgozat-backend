package pte.mik.szakdolgozat.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.mongodb.repository.Tailable;

import pte.mik.szakdolgozat.entity.Email;
import reactor.core.publisher.Flux;

public interface EmailRepository extends ReactiveMongoRepository<Email, String> {
    @Tailable
    Flux<Email> getEmailsBy();

    @Tailable
    Flux<Email> getEmailsByConversationId(String conversationId);
}
