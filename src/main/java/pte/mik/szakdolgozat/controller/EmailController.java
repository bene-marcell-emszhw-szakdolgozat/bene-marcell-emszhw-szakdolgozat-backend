package pte.mik.szakdolgozat.controller;

import java.security.Principal;

import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.*;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pte.mik.szakdolgozat.entity.Email;
import pte.mik.szakdolgozat.service.EmailService;
import pte.mik.szakdolgozat.service.SSEService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/email")
public class EmailController {

    private final EmailService emailService;
    private final SSEService sseService;

    @GetMapping(
            path="/listLoggedInUsersReceivedEmails",
            produces = MediaType.TEXT_EVENT_STREAM_VALUE
    )
    public Flux<ServerSentEvent<Email>> listLoggedInUsersReceivedEmails(Principal principal) {
        return this.sseService.wrap(
                this.emailService.listLoggedInUsersReceivedEmails(principal.getName())
        );
    }

    @GetMapping(path="/listLoggedInUsersSentEmails", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<ServerSentEvent<Email>> listLoggedInUsersSentEmails(Principal principal) {
        return this.sseService.wrap(this.emailService.listLoggedInUsersSentEmails(principal.getName()));
    }

    @GetMapping("/listEmailsByConversationId/{conversationId}")
    public Flux<ServerSentEvent<Email>> listEmailsByConversationId(@PathVariable String conversationId) {
        return this.sseService.wrap(this.emailService.findEmailsByConversationId(conversationId));
    }

    @PostMapping("/sendMail")
    public Mono<Email> sendMail(@RequestBody Email email, Principal principal) {
        return emailService.sendMail(email,principal.getName());
    }

}
