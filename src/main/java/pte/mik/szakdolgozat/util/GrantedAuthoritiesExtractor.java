package pte.mik.szakdolgozat.util;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.ReactiveJwtAuthenticationConverterAdapter;

import reactor.core.publisher.Mono;

public class GrantedAuthoritiesExtractor {

    @Value("${keycloak-props-props.user-client-name}")
    private String keycloakUserClientName;

    public Converter<Jwt, Mono<AbstractAuthenticationToken>> grantedAuthoritiesExtractor() {

        JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(jwt -> {

            Map<String, Object> resourceAccessClaim = jwt.hasClaim("resource_access")
                    ? jwt.getClaimAsMap("resource_access")
                    : Collections.emptyMap();

            @SuppressWarnings("unchecked")
            Collection<?> authorities = ((Map<String, List<String>>) resourceAccessClaim.getOrDefault(
                    this.keycloakUserClientName, Collections.EMPTY_MAP)).getOrDefault("roles", Collections.EMPTY_LIST);

            return authorities.stream()
                              .map(Object::toString)
                              .map(SimpleGrantedAuthority::new)
                              .collect(Collectors.toList());

        });
        return new ReactiveJwtAuthenticationConverterAdapter(jwtAuthenticationConverter);
    }
}
