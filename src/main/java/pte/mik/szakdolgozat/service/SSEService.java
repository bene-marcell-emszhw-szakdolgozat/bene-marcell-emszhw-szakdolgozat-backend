package pte.mik.szakdolgozat.service;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Flux;

@Service
public class SSEService {

    private final long delay;

    public SSEService(@Value("${sse-service.keep-alive-comment-freq}") long delay) {
        this.delay = delay;
    }

    public <T> Flux<ServerSentEvent<T>> wrap(Flux<T> flux) {
        return Flux.merge(flux.map(t -> ServerSentEvent.builder(t).build()),
                Flux.interval(Duration.ofMillis(this.delay))
                    .map(aLong -> ServerSentEvent.<T>builder().comment("keep alive").build()));
    }
}
