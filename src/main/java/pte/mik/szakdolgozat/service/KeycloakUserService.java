package pte.mik.szakdolgozat.service;

import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.RequiredArgsConstructor;
import pte.mik.szakdolgozat.entity.User;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class KeycloakUserService {

    private final WebClient keycloakWebClient;

    public Flux<User> getAllRegisteredUser() {
        return this.keycloakWebClient.get().uri("/users").retrieve().bodyToFlux(User.class);
    }

    public Mono<User> getUserById(String userId) {
        return this.keycloakWebClient.get().uri("/users/" + userId).retrieve().bodyToMono(User.class);
    }

}
