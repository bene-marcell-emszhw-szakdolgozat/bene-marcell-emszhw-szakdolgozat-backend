package pte.mik.szakdolgozat.service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.activation.DataSource;
import javax.mail.Address;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.mail.util.MimeMessageParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import dev.snowdrop.vertx.mail.MailClient;
import dev.snowdrop.vertx.mail.MailMessage;
import dev.snowdrop.vertx.mail.SimpleMailAttachment;
import dev.snowdrop.vertx.mail.SimpleMailMessage;
import lombok.extern.slf4j.Slf4j;
import pte.mik.szakdolgozat.entity.Attachment;
import pte.mik.szakdolgozat.entity.Email;
import pte.mik.szakdolgozat.entity.User;
import pte.mik.szakdolgozat.repository.EmailRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class EmailService {

    private final String domainName;
    private final MailClient mailClient;
    private final EmailRepository emailRepository;
    private final DataBufferFactory dataBufferFactory;
    private final KeycloakUserService keycloakUserService;

    public EmailService(@Value("${domain-name}") String domainName, MailClient mailClient,
            EmailRepository emailRepository, DataBufferFactory dataBufferFactory,
            KeycloakUserService keycloakUserService) {
        this.domainName = domainName;
        this.mailClient = mailClient;
        this.emailRepository = emailRepository;
        this.dataBufferFactory = dataBufferFactory;
        this.keycloakUserService = keycloakUserService;
    }

    public Flux<Email> listLoggedInUsersReceivedEmails(String loggedInUserId) {
        return this.keycloakUserService.getUserById(loggedInUserId)
                                       .flatMapMany(
                                               loggedInUser -> this.emailRepository.getEmailsBy()
                                                                                        .filter(email -> emailContainsUsernameReceived(email,loggedInUser)));
    }

    public Flux<Email> listLoggedInUsersSentEmails(String loggedInUserId) {
        return this.keycloakUserService.getUserById(loggedInUserId).flatMapMany(
                loggedInUserEmail -> this.emailRepository.getEmailsBy()
                        .filter(email -> email.getFrom()
                                        .contains(loggedInUserEmail.getUsername())
                        )
        );
    }

    public Mono<Email> save(Email email) {

        email.setSentDate(LocalDateTime.now());

        // Ha bármelyik field null volna, akkor inicializálja azt
        if (email.getInReplyTo() == null) {
            email.setInReplyTo(new ArrayList<>());
        }
        if (email.getReplyReferences() == null) {
            email.setReplyReferences(new ArrayList<>());
        }
        if (email.getSubject() == null) {
            email.setSubject("");
        }
        if (email.getFrom() == null) {
            email.setFrom("");
        }
        if (email.getTo() == null) {
            email.setTo(new ArrayList<>());
        }
        if (email.getCc() == null) {
            email.setCc(new ArrayList<>());
        }
        if (email.getBcc() == null) {
            email.setBcc(new ArrayList<>());
        }
        if (email.getPlainContent() == null) {
            email.setPlainContent("");
        }
        if (email.getHtmlContent() == null) {
            email.setHtmlContent("");
        }
        if (email.getAttachments() == null) {
            email.setAttachments(new ArrayList<>());
        }

        if (CollectionUtils.isNotEmpty(email.getReplyReferences())) {
            this.findById(email.getReplyReferences().get(0)).doOnNext(next->email.setConversationId(next.getConversationId())).subscribe();
        } else {
            email.setConversationId(UUID.randomUUID().toString());
        }

        // Ha már létezik a db-ben, vagy egyik user-hez se kapcsolható az email,
        // akkor ne mentse le a db-be
        return this.emailRepository.existsById(email.getId())
                .filter(booleanVal->!booleanVal)
                .flatMap(
                        exists -> this.keycloakUserService.getAllRegisteredUser()
                                .filter(user -> this.emailContainsUsername(
                                        email, user))
                                .hasElements()
                                .filter(Boolean::booleanValue)
                                .flatMap(
                                        hasElements -> this.emailRepository.save(
                                                email)));



    }

    public Flux<Email> listAll() {
        return this.emailRepository.findAll();
    }

    public Mono<Email> findById(String email) {
        return this.emailRepository.findById(email);
    }

    public Flux<Email> findEmailsByConversationId(String conversationId) {
        return this.emailRepository.getEmailsByConversationId(conversationId);
    }

    public Mono<Email> sendMail(Email email, String userId) {

        // @formatter:off
        return this.keycloakUserService.getUserById(userId)
                .map(userRepresentation -> {
                    email.setId(this.generateId());
                    email.setFrom(userRepresentation.getUsername());
                    return this.emailToMailMessage(email);
                })
                .flatMap(mailMessage ->
                        this.mailClient
                                .send(mailMessage)
                                .flatMap(map->this.save(email))
                );

        //@formatter:on
    }

    public Email inputStreamToEmail(InputStream data) throws Exception {
        MimeMessage message = new MimeMessage(Session.getDefaultInstance(new Properties()), data);

        MimeMessageParser mimeMessageParser = new MimeMessageParser(message);
        mimeMessageParser.parse();

        List<String> replyReferencesList = message.getHeader("References") == null ? new ArrayList<>()
                : new ArrayList<>(Arrays.asList(message.getHeader("References", null).split("\\s+")));

        List<String> inReplyToList = message.getHeader("In-Reply-To") == null ? new ArrayList<>()
                : new ArrayList<>(Arrays.asList(message.getHeader("In-Reply-To", null).split("\\s+")));

        List<Attachment> attachmentList = new ArrayList<>();

        for (DataSource ds : mimeMessageParser.getAttachmentList()) {
            attachmentList.add(new Attachment(ds.getInputStream().readAllBytes(), ds.getContentType(), ds.getName()));
        }

        Email email = new Email();

        email.setId(Objects.toString(message.getMessageID(),this.generateId()));
        email.setReplyReferences(replyReferencesList); // Ő se lehet null
        email.setInReplyTo(inReplyToList); // Ő se lehet null
        email.setSubject(Objects.toString(mimeMessageParser.getSubject(),""));
        email.setFrom(Objects.toString(mimeMessageParser.getFrom(),""));
        email.setTo(mimeMessageParser.getTo().stream().map(Address::toString).collect(Collectors.toList()));    // null esetén is üres tömböt ad vissza a .getTo()
        email.setCc(mimeMessageParser.getCc().stream().map(Address::toString).collect(Collectors.toList()));    // null esetén is üres tömböt ad vissza a .getCc()
        email.setBcc(mimeMessageParser.getBcc().stream().map(Address::toString).collect(Collectors.toList()));  // null esetén is üres tömböt ad vissza a .getBcc()
        email.setPlainContent(Objects.toString(mimeMessageParser.getPlainContent(),""));
        email.setHtmlContent(Objects.toString(mimeMessageParser.getHtmlContent(),""));
        email.setAttachments(attachmentList); // Ő se lehet null

        return email;
    }

    private String generateId() {
        return "<" + Instant.now().toEpochMilli() + "-" + UUID.randomUUID() + "@" + this.domainName + ">";
    }

    private MailMessage emailToMailMessage(Email email) {

        SimpleMailMessage message = new SimpleMailMessage();

        MultiValueMap<String, String> messageHeaders = new LinkedMultiValueMap<>();

        messageHeaders.add("Message-ID", email.getId());
        if (email.getInReplyTo() != null) {
            messageHeaders.add("In-Reply-To", String.join("\\r\\n", email.getInReplyTo()));
        }
        if (email.getReplyReferences() != null) {
            messageHeaders.add("References", String.join("\\r\\n", email.getReplyReferences()));
        }

        message.setHeaders(messageHeaders);

        message.setSubject(email.getSubject());
        message.setFrom(email.getFrom());
        message.setTo(email.getTo());
        message.setCc(email.getCc());
        message.setBcc(email.getBcc());
        message.setText(
                email.getHtmlContent() == null ?
                "" :
                email.getHtmlContent().replaceAll("<[^>]*>","")
        );
        message.setHtml(email.getHtmlContent());

        if (email.getAttachments() != null) {
            email.getAttachments().forEach(attachment -> {
                SimpleMailAttachment simpleMailAttachment = new SimpleMailAttachment();

                simpleMailAttachment.setContentType(attachment.getContentType());
                simpleMailAttachment.setName(attachment.getName());
                simpleMailAttachment.setData(DataBufferUtils.readInputStream(
                        () -> new ByteArrayInputStream(attachment.getData()), this.dataBufferFactory, 4096));

                message.addAttachment(simpleMailAttachment);
            });
        }

        return message;
    }

    private boolean emailContainsUsername(Email email, User user) {
        return Stream.concat(
                Stream.of(email.getTo(), email.getCc(), email.getBcc()).flatMap(Collection::stream),
                Stream.of(email.getFrom())
        ).anyMatch(mail -> user.getUsername().equals(mail));
    }

    private boolean emailContainsUsernameReceived(Email email, User loggedInUser) {
        return Stream.of(email.getTo(), email.getCc(), email.getBcc()).flatMap(Collection::stream).anyMatch(mail->loggedInUser.getUsername().equals(mail));
    }
}
