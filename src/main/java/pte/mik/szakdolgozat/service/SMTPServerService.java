package pte.mik.szakdolgozat.service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.stereotype.Service;
import org.subethamail.smtp.server.SMTPServer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class SMTPServerService {

    private final SMTPServer smtpServer;

    @PostConstruct
    public void start() {
        this.smtpServer.start();
        log.info("SMTP server is listening...");
    }

    @PreDestroy
    public void stop() {
        this.smtpServer.stop();
    }
}
