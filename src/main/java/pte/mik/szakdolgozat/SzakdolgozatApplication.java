package pte.mik.szakdolgozat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.reactive.ReactiveUserDetailsServiceAutoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@EnableReactiveMongoRepositories
@SpringBootApplication(exclude = {ReactiveUserDetailsServiceAutoConfiguration.class})
public class SzakdolgozatApplication {

    public static void main(String[] args) {
        SpringApplication.run(SzakdolgozatApplication.class, args);
    }
}