package pte.mik.szakdolgozat.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.Assert;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.snowdrop.vertx.mail.MailClient;
import dev.snowdrop.vertx.mail.MailResult;
import pte.mik.szakdolgozat.entity.Email;
import pte.mik.szakdolgozat.entity.User;
import pte.mik.szakdolgozat.repository.EmailRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@ExtendWith(MockitoExtension.class)
class EmailServiceTest {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final List<Email> emailListDump = new ArrayList<>();

    @Mock
    private EmailRepository emailRepository;

    @Mock
    private MailClient mailClient;

    @Mock
    private KeycloakUserService keycloakUserService;

    @InjectMocks
    private EmailService emailService;

    private User loggedInUser;

    @BeforeEach
    void init() throws IOException {
        this.objectMapper.findAndRegisterModules();
        this.emailListDump.addAll(List.of(
                this.objectMapper.readValue(new File("src/test/resources/mongo_dump_test.json"), Email[].class)));
        this.loggedInUser = User.builder()
                                .id("4f841e15-e610-4f8a-af72-83cf3cd64012")
                                .username("benmarcll@emszhw-szakdolgozat.email")
                                .firstName("Marcell")
                                .lastName("Bene")
                                .email("benmarcll@emszhw-szakdolgozat.email")
                                .emailVerified(true)
                                .build();

    }

    @Test
    void listAll() {
        Mockito.when(this.emailRepository.findAll()).thenReturn(Flux.fromIterable(this.emailListDump));

        StepVerifier.create(this.emailService.listAll())
                    .expectNext(this.emailListDump.get(0), this.emailListDump.get(1), this.emailListDump.get(2),
                            this.emailListDump.get(3), this.emailListDump.get(4))
                    .expectComplete()
                    .verify();
    }

    @Test
    void listLoggedInUsersReceivedEmails() {
        Mockito.when(this.keycloakUserService.getUserById("4f841e15-e610-4f8a-af72-83cf3cd64012"))
               .thenReturn(Mono.just((this.loggedInUser)));

        Mockito.when(this.emailRepository.getEmailsBy()).thenReturn(Flux.fromIterable(this.emailListDump));

        StepVerifier.create(this.emailService.listLoggedInUsersReceivedEmails("4f841e15-e610-4f8a-af72-83cf3cd64012"))
                    .expectNext(this.emailListDump.get(0), this.emailListDump.get(1), this.emailListDump.get(4))
                    .expectComplete()
                    .verify();
    }

    @Test
    void listLoggedInUserSentEmails() {
        Mockito.when(this.keycloakUserService.getUserById("4f841e15-e610-4f8a-af72-83cf3cd64012"))
               .thenReturn(Mono.just((this.loggedInUser)));

        Mockito.when(this.emailRepository.getEmailsBy()).thenReturn(Flux.fromIterable(this.emailListDump));

        StepVerifier.create(this.emailService.listLoggedInUsersSentEmails("4f841e15-e610-4f8a-af72-83cf3cd64012"))
                    .expectNext(this.emailListDump.get(2), this.emailListDump.get(3))
                    .expectComplete()
                    .verify();
    }

    @Test
    void incomingEmailTest() {

        // Bejövő e-mail
        Email incomingEmail = Email.builder()
                                   .id("<teszt_id@teszt.com>")
                                   .subject("Teszt_subject")
                                   .from("from@teszt.hu")
                                   .replyReferences(List.of("<reference_id@teszt.com>"))
                                   .to(List.of("benmarcll@emszhw-szakdolgozat.email"))
                                   .cc(List.of("cc@cc.hu"))
                                   .htmlContent("<div><b>random Üzenet</b></div>")
                                   .plainContent("random Üzenet")
                                   .build();

        // E-mail, amelyre válaszol a bejövő e-mail
        Email referenceEmail = Email.builder()
                                    .id("<reference_id@teszt.com>")
                                    .conversationId("30dd0bc7-1374-4623-8d7e-a26f7991acca")
                                    .build();

        Mockito.when(this.emailRepository.existsById("<teszt_id@teszt.com>")).thenReturn(Mono.just(false));

        Mockito.when(this.emailRepository.findById("<reference_id@teszt.com>")).thenReturn(Mono.just(referenceEmail));

        Mockito.when(this.keycloakUserService.getAllRegisteredUser()).thenReturn(Flux.just(this.loggedInUser));

        Mockito.when(this.emailRepository.save(incomingEmail)).thenReturn(Mono.just(incomingEmail));

        StepVerifier.create(this.emailService.save(incomingEmail))
                    .expectNextMatches(next -> next.equals(Email.builder()
                                                                .id("<teszt_id@teszt.com>")
                                                                .subject("Teszt_subject")
                                                                .inReplyTo(new ArrayList<>())
                                                                .replyReferences(List.of("<reference_id@teszt.com>"))
                                                                .attachments(new ArrayList<>())
                                                                .from("from@teszt.hu")
                                                                .to(List.of("benmarcll@emszhw-szakdolgozat.email"))
                                                                .cc(List.of("cc@cc.hu"))
                                                                .bcc(new ArrayList<>())
                                                                .htmlContent("<div><b>random Üzenet</b></div>")
                                                                .plainContent("random Üzenet")
                                                                .conversationId("30dd0bc7-1374-4623-8d7e-a26f7991acca")
                                                                // sentDate beállítása itt muszáj volt, mivel a
                                                                // LocalDateTime.now() függvényt használom a
                                                                // service osztályom save method-jában
                                                                .sentDate(next.getSentDate())
                                                                .build()))
                    .expectComplete()
                    .verify();

    }

    @Test
    void outgoingEmailTest() {
        Email outgoingEmail = Email.builder()
                                   .sentDate(LocalDateTime.now())
                                   .inReplyTo(new ArrayList<>())
                                   .replyReferences(new ArrayList<>())
                                   .subject("Teszt_subject")
                                   .to(List.of("teszt@teszt.hu"))
                                   .cc(List.of("cc@cc.hu"))
                                   .bcc(new ArrayList<>())
                                   .plainContent("teszt Üzenet")
                                   .htmlContent("<div><b>teszt Üzenet</b></div>")
                                   .attachments(new ArrayList<>())
                                   .build();

        Mockito.when(this.keycloakUserService.getUserById("4f841e15-e610-4f8a-af72-83cf3cd64012"))
               .thenReturn(Mono.just((this.loggedInUser)));

        Mockito.when(this.mailClient.send(Mockito.any())).thenReturn(Mono.just(new MailResult() {
            @Override
            public String getMessageId() {
                return null;
            }

            @Override
            public List<String> getRecipients() {
                return null;
            }
        }));

        Mockito.when(this.emailRepository.existsById(Mockito.anyString())).thenReturn(Mono.just(false));

        Mockito.when(this.keycloakUserService.getAllRegisteredUser()).thenReturn(Flux.just(this.loggedInUser));

        Mockito.when(this.emailRepository.save(outgoingEmail)).thenReturn(Mono.just(outgoingEmail));

        StepVerifier.create(this.emailService.sendMail(outgoingEmail, this.loggedInUser.getId()))
                    .expectNextMatches(matchEmail -> matchEmail.getId() != null
                            && matchEmail.getFrom().equals("benmarcll@emszhw-szakdolgozat.email")
                            && matchEmail.getConversationId() != null)
                    .expectComplete()
                    .verify();
    }

    @Test
    void save() {
        Email email = Email.builder()
                           .id("test_id")
                           .from("benmarcll@emszhw-szakdolgozat.email")
                           .to(List.of("to@email.test"))
                           .subject("test subject")
                           .build();

        Mockito.when(this.emailRepository.existsById(Mockito.anyString())).thenReturn(Mono.just(false));

        Mockito.when(this.keycloakUserService.getAllRegisteredUser()).thenReturn(Flux.just(this.loggedInUser));

        Mockito.when(this.emailRepository.save(email)).thenReturn(Mono.just(email));

        StepVerifier.create(this.emailService.save(email))
                    // Nem null egyik field-je sem
                    .expectNextMatches(matchEmail -> ObjectUtils.allNotNull(matchEmail.getInReplyTo(),
                            matchEmail.getReplyReferences(), matchEmail.getSubject(), matchEmail.getFrom(),
                            matchEmail.getTo(), matchEmail.getCc(), matchEmail.getBcc(), matchEmail.getPlainContent(),
                            matchEmail.getHtmlContent(), matchEmail.getAttachments(), matchEmail.getConversationId(),
                            matchEmail.getSentDate()))
                    .expectComplete()
                    .verify();

    }

    @Test
    void inputStreamToEmail() throws Exception {

        Path path = Paths.get("src/test/resources/incoming_email_inputstream_content");

        String string = StringUtils.join(Files.readAllLines(path), "\r\n");

        InputStream inputStream = new ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8));

        Email email = this.emailService.inputStreamToEmail(inputStream);

        Assert.notNull(email.getReplyReferences(), "Reply reference is null");
        Assert.notNull(email.getInReplyTo(), "In reply to is null");
        Assert.notNull(email.getAttachments(), "Attachment is null");
        Assert.notNull(email.getId(), "Id is null");
        Assert.notNull(email.getSubject(), "Subject is null");
        Assert.notNull(email.getFrom(), "From is null");
        Assert.notNull(email.getTo(), "To is null");
        Assert.notNull(email.getCc(), "Cc is null");
        Assert.notNull(email.getBcc(), "Bcc is null");
        Assert.notNull(email.getPlainContent(), "Plain content is null");
        Assert.notNull(email.getHtmlContent(), "HTML content is null");

    }

}
